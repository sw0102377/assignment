<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// Route::get('/', function () {
//     return view('auth2.login');
// });

Auth::routes();

Route::resource('house', 'HouseUnitController');
Route::resource('tenant', 'TenantController');
Route::resource('userdetail', 'UserDetailsController');

Route::resource('userlevel', 'UserlevelController');