@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            
            <h1 class="display-3">Tenant Details</h1>
            <div class="row">
                <div class="card">
                    <div class="card-header bg-default">
                        <h2> Tenant: {{$tenant->name}}</h2>
                    </div>
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-3">Name</dt>
                            <dd class="col-sm-9">{{$tenant->name}}</dd>
                            
                            <dt class="col-sm-3">IC/Passport</dt>
                            <dd class="col-sm-9">{{$tenant->passport}}</dd>
                            
                            <dt class="col-sm-3">Citizenship</dt>
                            <dd class="col-sm-9">{{ $tenant->citizenship }}</dd>
                            
                            <dt class="col-sm-3">Phone</dt>
                            <dd class="col-sm-9">{{$tenant->phone}}</dd>

                            <dt class="col-sm-3">Duration</dt>
                            <dd class="col-sm-9">{{$tenant->duration}} Days</dd>

                            <dt class="col-sm-3">Rented on:</dt>
                            <dd class="col-sm-9">{{$tenant->created_at}}</dd>

                            <a href="{{route('tenant.edit', $tenant->id)}}" class="btn btn-primary">edit</a>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8 offset-sm-2">
            
                <h1 class="display-7">Rented Unit Details</h1>
                <div class="row">
                    <div class="card">
                        <div class="card-header bg-default">
                            <h5> Owner: {{$tenant->houseunits->user['name']}}</h5>
                        </div>
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-sm-3">Owner</dt>
                                <dd class="col-sm-9">{{$tenant->houseunits->user['name']}}</dd>
                                
                                <dt class="col-sm-3">House Number</dt>
                                <dd class="col-sm-9">{{$tenant->houseunits['housenumber']}}</dd>
                                
                                <dt class="col-sm-3">Level</dt>
                                <dd class="col-sm-9">{{$tenant->houseunits['level']}}</dd>
                                
                                <dt class="col-sm-3">Block</dt>
                                <dd class="col-sm-9">{{$tenant->houseunits['block']}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    
@endsection