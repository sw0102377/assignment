@extends('main.layout')

@section('bootstrap')
<link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.min.css">
<link href="/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
<link rel="stylesheet" href="/assets/libs/css/style.css">
<link rel="stylesheet" href="/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
<link rel="stylesheet" href="/assets/vendor/charts/morris-bundle/morris.css">
<link rel="stylesheet" href="/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
@endsection

@section('wrapper')
<div class="dashboard-wrapper">
    <div class="dashboard-influence">
        <div class="container-fluid dashboard-content">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header">
                        <h3 class="mb-2">{{$userD->user['name']}} Profile</h3>
                        <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">{{$userD->user['name']}} Profile</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- content  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- influencer profile  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        @if (session()->get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session()->get('success') }}
                        </div>
                        @elseif(session()->get('danger'))
                        <div class="alert alert-danger">
                            {{ session()->get('danger') }}
                        </div>
                        @endif
                    <div class="card influencer-profile-data">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-12">
                                    <div class="text-center">
                                        <img src="/assets/images/avatar-1.jpg" alt="User Avatar" class="rounded-circle user-avatar-xxl">
                                        </div>
                                    </div>
                                    <div class="col-xl-10 col-lg-8 col-md-8 col-sm-8 col-12">
                                        <div class="user-avatar-info">
                                            <div class="m-b-20">
                                                <div class="user-avatar-name">
                                                    <h2 class="mb-1">{{ $userD->user['name']}}</h2>
                                                </div>
                                                <div class="rating-star  d-inline-block">
                                                </div>
                                            </div>
                                            <!--  <div class="float-right"><a href="#" class="user-avatar-email text-secondary">www.henrybarbara.com</a></div> -->
                                            <div class="user-avatar-address">
                                                <p class="border-bottom pb-3">
                                                    <span class="d-xl-inline-block d-block mb-2"><i class="fab fa-gofore mr-2 text-primary "></i>{{$userD->email}}</span>
                                                    <span class="mb-2 ml-xl-4 d-xl-inline-block d-block"><i class="fas fa-phone mr-2 text-primary "></i>{{$userD->phone}}</span>
                                                    <span class=" mb-2 d-xl-inline-block d-block ml-xl-4">
                                                        <i class="fas fa-flag mr-2 text-primary "></i>{{$userD->citizenship}}</span>
                                                    <span class=" mb-2 d-xl-inline-block d-block ml-xl-4"><i class="far fa-calendar-alt mr-2 text-primary "></i>Joined date: {{$userD->user['created_at']}}</span>
                                                </p>
                                                <div class="mt-3">
                                                    <a href=" {{route('userdetail.edit', $userD->user['id'])}}" class="btn btn-secondary">Edit Profile</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end influencer profile  -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- widgets   -->
                <!-- ============================================================== -->
                

                <div class="row">
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-block">
                            <h3 class="section-title">{{ $userD->user['name']}}'s House(s)</h3>
                        </div>
                    </div>
                    @foreach ($houses as $h)
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="card campaign-card text-center">
                            <div class="card-body">
                                <div class="campaign-img"><img src="/assets/images/dribbble.png" alt="user" class="user-avatar-xl"></div>
                                    <div class="campaign-info">
                                        <h3 class="mb-1">{{$h->block}}-{{$h->level}}-{{$h->housenumber}}</h3>
                                        <p class="mb-3"></p>
                                        <p class="mb-1">Status:<span class="text-dark font-medium ml-2">@if ($h->status==1)
                                            Rented</span></p>
                                            <p>Tenant: @foreach ($tenant->where('house_id', $h->id) as $t)
                                                <span class="text-dark font-medium ml-2">{{$t->name}}</span></p>
                                            @endforeach
                                        @else
                                            Own Stay</span></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- campaign activities   -->
                    <!-- ============================================================== -->
                    <div class="col-lg-12">
                        <div class="section-block">
                            <h3 class="section-title">{{ $userD->user['name']}}'s Tenants List</h3>
                        </div>
                        <div class="card">
                            <div class="campaign-table table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr class="border-0">
                                            <th class="border-0">Name</th>
                                            <th class="border-0">IC/Passport</th>
                                            <th class="border-0">Citizenship</th>
                                            <th class="border-0">phone</th>
                                            <th class="border-0">House Rented</th>
                                            <th class="border-0">Duration(days)</th>
                                            <th class="border-0">Date Entered</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($tenant as $t)
                                        <tr>
                                        <td>{{$t->name}}</td>
                                        <td>{{$t->passport}}</td>
                                        <td>{{$t->citizenship}}</td>
                                        <td>{{$t->phone}}</td>
                                        <td>{{$t->houseunits['block']}}-{{$t->houseunits['level']}}-{{$t->houseunits['housenumber']}}</td>
                                        <td>{{$t->duration}}</td>
                                        <td>{{$t->created_at}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end campaign activities   -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- recommended campaigns   -->
                <!-- ============================================================== -->


                            </div>
                        </div>

                    </div>
@endsection

@section('javascript')
	                    <script src="/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
	                    <!-- bootstap bundle js -->
	                    <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
	                    <!-- slimscroll js -->
	                    <script src="/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
	                    <!-- main js -->
	                    <script src="/assets/libs/js/main-js.js"></script>
	                    <!-- morris-chart js -->
	                    <script src="/assets/vendor/charts/morris-bundle/raphael.min.js"></script>
	                    <script src="/assets/vendor/charts/morris-bundle/morris.js"></script>
	                    <script src="/assets/vendor/charts/morris-bundle/morrisjs.html"></script>
	                    <!-- chart js -->
	                    <script src="/assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>
	                    <script src="/assets/vendor/charts/charts-bundle/chartjs.js"></script>
	                    <!-- dashboard js -->
	                    <script src="/assets/libs/js/dashboard-influencer.js"></script>
@endsection

{{-- @extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            
            <h1 class="display-3">Update User</h1>
            <form method="POST" action="{{ route('userdetail.update', $userD->id) }}">
            @csrf
            @method('PATCH')
                <div class="row">
                    <div class="col-sn-12">
                        <div class="card">
                            <div class="card-header bg-default">
                                <h2>User: {{$userD->user['name'] }}</h2>
                            </div>
                            <div class="card-body">
                                    @if ($errors->any())
                                    <div class="alert-danger">
                                        <ul>@foreach($errors->all() as $error)
                                            <li>{{ $error  }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                <dl class="row">

                                    <dt class="col-sm-3">Name</dt>
                                    <dd class="col-sm-9"><input type="text" name="name" value="{{$userD->user['name']}}" size="50"></dd>
                                    
                                    <dt class="col-sm-3">IC/Passport</dt>
                                    <dd class="col-sm-9"><input type="text" name="passport" value="{{$userD->user['passport']}}" size="50"></dd>
                                    
                                    <dt class="col-sm-3">Citizenship</dt>
                                    <dd class="col-sm-9"><input type="text" name="citizenship" value="{{ $userD->citizenship }}" size="50"></dd>
                                    
                                    <dt class="col-sm-3">Phone</dt>
                                    <dd class="col-sm-9"><input type="text" name="phone" value="{{ $userD->phone }}" size="50"></dd>
        
                                    <dt class="col-sm-3">Email</dt>
                                    <dd class="col-sm-9"><input type="text" name="email" value="{{$userD->email}}" size="50"></dd>

                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sn-12 text-center">
                        <button type="submit" class="btn btn-primary">Update changes</button>
                        <a class="btn btn-warning" href="{{route('house.index')}}">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
@endsection --}}
{{-- @extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            
            <h1 class="display-3">User Details</h1>
            <div class="row">
                <div class="card">
                    <div class="card-header bg-default">
                        <h2> User: {{$userD->user['name']}}</h2>
                    </div>
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-3">Name</dt>
                            <dd class="col-sm-9">{{$userD->user['name']}}</dd>
                            
                            <dt class="col-sm-3">IC/Passport</dt>
                            <dd class="col-sm-9">{{$userD->user['passport']}}</dd>
                            
                            <dt class="col-sm-3">Citizenship</dt>
                            <dd class="col-sm-9">{{ $userD->citizenship }}</dd>
                            
                            <dt class="col-sm-3">Phone</dt>
                            <dd class="col-sm-9">{{$userD->phone}}</dd>

                            <dt class="col-sm-3">Email</dt>
                            <dd class="col-sm-9">{{$userD->email}}</dd>

                            <a href="{{route('userdetail.edit', $userD->id)}}" class="btn btn-primary">edit</a>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection --}}