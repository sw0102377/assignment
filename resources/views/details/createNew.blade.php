@extends('main.layout')

@section('bootstrap')
<link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/libs/css/style.css">
    <link rel="stylesheet" href="/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
@endsection

@section('wrapper')
<div class="dashboard-wrapper">
        <div class="container-fluid  dashboard-content">
         
                <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">Fill Up Your Details First!</h2>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic form -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                               <h5 class="card-header">Details About You!</h5>
                               @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>@foreach($errors->all() as $error)
                                        <li>{{ $error  }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            <div class="card-body">
                                <form method="POST" action="{{ route('userdetail.store') }}" id="basicform" data-parsley-validate="">
                                    @csrf
                                    <div class="form-group">
                                        <label for="inputCiti">Citizenship</label>
                                        <input id="inputCiti" name="citizenship" type="text" placeholder="Citizenship.." class="form-control">
                                    </div>
                                    <div class="form-group">
                                            <label for="inputPhone">Phone</label>
                                            <input id="inputPhone" name="phone" type="text" placeholder="Phone Number.." class="form-control">
                                        </div>
                                    <div class="form-group">
                                        <label for="inputEmail">Email</label>
                                        <input id="inputEmail" name="email" type="email" placeholder="Email.." class="form-control">
                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="text-right">
                                            <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                            <button class="btn btn-space btn-secondary"><a href="{{route('house.index')}}">Cancel</a></button>
                                        </p>
                                    </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic form -->
                    <!-- ============================================================== -->
                </div>
       
        </div>
@endsection

@section('javascript')
<script src="/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="/assets/vendor/parsley/parsley.js"></script>
    <script src="/assets/libs/js/main-js.js"></script>
    <script>
    $('#form').parsley();
    </script>
    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    </script>
@endsection

{{-- @extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            
            <h1 class="display-3">Update User</h1>
            <form method="POST" action="{{ route('userdetail.update', $userD->id) }}">
            @csrf
            @method('PATCH')
                <div class="row">
                    <div class="col-sn-12">
                        <div class="card">
                            <div class="card-header bg-default">
                                <h2>User: {{$userD->user['name'] }}</h2>
                            </div>
                            <div class="card-body">
                                    @if ($errors->any())
                                    <div class="alert-danger">
                                        <ul>@foreach($errors->all() as $error)
                                            <li>{{ $error  }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                <dl class="row">

                                    <dt class="col-sm-3">Name</dt>
                                    <dd class="col-sm-9"><input type="text" name="name" value="{{$userD->user['name']}}" size="50"></dd>
                                    
                                    <dt class="col-sm-3">IC/Passport</dt>
                                    <dd class="col-sm-9"><input type="text" name="passport" value="{{$userD->user['passport']}}" size="50"></dd>
                                    
                                    <dt class="col-sm-3">Citizenship</dt>
                                    <dd class="col-sm-9"><input type="text" name="citizenship" value="{{ $userD->citizenship }}" size="50"></dd>
                                    
                                    <dt class="col-sm-3">Phone</dt>
                                    <dd class="col-sm-9"><input type="text" name="phone" value="{{ $userD->phone }}" size="50"></dd>
        
                                    <dt class="col-sm-3">Email</dt>
                                    <dd class="col-sm-9"><input type="text" name="email" value="{{$userD->email}}" size="50"></dd>

                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sn-12 text-center">
                        <button type="submit" class="btn btn-primary">Update changes</button>
                        <a class="btn btn-warning" href="{{route('house.index')}}">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
@endsection --}}