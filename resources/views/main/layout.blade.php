<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    @yield('bootstrap')
    <title>Zuhudy.com</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
        function a(){
            $("#a").hide();
            $('#status').on('change', function() {
                if ( this.value == '1')
                {
                    $("#a").show();
                }
                else
                {
                    $("#a").hide();
                }
            });
        };
    </script>
</head>

<body onload="a()">
        

    <!-- main wrapper -->
    <div class="dashboard-main-wrapper">

        <!-- navbar -->
        @include('main.navbar')
        <!-- end navbar -->

        <!-- left sidebar -->
        @include('main.leftsidebar')
        <!-- end left sidebar -->

        <!-- wrapper  -->
            @yield('wrapper')
        <!-- end wrapper  -->
        
    </div>
    <!-- end main wrapper  -->

    
    <!-- Optional JavaScript -->
    @yield('javascript')
</body>
 
</html>