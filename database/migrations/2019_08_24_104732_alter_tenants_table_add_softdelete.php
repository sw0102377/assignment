<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTenantsTableAddSoftdelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenants', function(Blueprint $table){
            $table->dropForeign(['house_id']);
            $table->dropColumn('house_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenants', function(Blueprint $table){
            $table->bigInteger('house_id')->unsigned()->index()->nullable();
            $table->foreign('house_id')->references('id')->on('house_units');
        });
    }
}
