<?php

namespace App\Http\Controllers;

use Auth;
use App\HouseUnit;
use App\Tenant;
use App\User;
use App\userDetails;
use Illuminate\Http\Request;
use App\Userlevel;
use Illuminate\Support\Facades\Gate;

class UserDetailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Gate::allows('admin-only', Auth::user()->userlevel->userLevel))
        {
            $userA=Userlevel::with('user.userDetails')->where('userLevel', '=', 'admin')->get();
            $userN=Userlevel::with('user.userDetails')->where('userLevel', '=', 'normal')->get();
            $i=1;
            return view('details.list', compact('userA', 'userN', 'i'));
        }
        return redirect('userdetail/'.Auth::user()->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(userDetails::where('user_id', '=', Auth::User()->id)->count() > 0){
            return $this->edit(Auth::User()->id);
        }
        return view('details.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'phone' => 'required|unique:user_Details',
            'citizenship' => 'required|min:1|max:100',
        ]);

        $usr= new userDetails();
        $usr->phone=$request->get('phone');
        $usr->citizenship=$request->get('citizenship');
        $usr->email=$request->get('email');

        $usrr=User::find(Auth::user()->id);
        $usr->user()->associate($usrr);

        $usr->save();

        return redirect('/house')->with('success', 'Your profile has been updated');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\userDetails  $userDetails
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        if(Gate::allows('admin-only', Auth::user()->userlevel->userLevel)||Auth::User()->id==$id){

            $userD=userDetails::with('user')->where('user_id', '=', $id)->first();
            $houses=HouseUnit::where('owner_id', '=', $id)->get();

            $h=HouseUnit::where('owner_id', '=', $id)->pluck('id')->toArray();
            $tenant=Tenant::with('houseunits')->wherein('house_id', $h)->get();

            
            // dd($user);
            return view('details.show', compact('userD', 'tenant', 'houses'));
        }
        else{
            return redirect('/userdetail/'.Auth::User()->id)->with('danger', 'Please Contact Admin first');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\userDetails  $userDetails
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('admin-only', Auth::User()->userlevel->userLevel)||Auth::User()->id==$id){
            //$userD=userDetails::with('user')->find($id)->first();
            $userD=userDetails::with('user')->where('user_id', '=', $id)->first();  
            return view('details.edit', compact('userD'));
        }
        return redirect('/userdetail/'.Auth::User()->id)->with('danger', 'Please Contact Admin first');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\userDetails  $userDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::allows('admin-only', Auth::User()->userlevel->userLevel)||Auth::User()->id==$id){
            $userD=userDetails::where('user_id', '=', $id)->first();
            $user=User::find($userD->user_id);

            // $request->validate([
            //     'name' => 'required | max:1',
            //     'email' => 'required | email',
            //     'department_id' => 'required'
            // ]);

            // $request->validate();

            
            $user->passport=$request->get('passport');
            $user->name=$request->get('name');
            $user->save();

            $userD->phone=$request->get('phone');
            $userD->email=$request->get('email');
            $userD->citizenship=$request->get('citizenship');
            $userD->save();

            $userD->user()->associate($user);

            

            return redirect('/userdetail/'.Auth::User()->id)->with('success', 'User has benn updated');
        }else{
            return redirect('/userdetail/'.Auth::User()->id)->with('danger', 'Please Contact Admin first');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\userDetails  $userDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
