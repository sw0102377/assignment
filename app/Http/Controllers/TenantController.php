<?php

namespace App\Http\Controllers;

use App\Tenant;
use Auth;
use App\HouseUnit;
use App\User;
use App\userDetails;
use Illuminate\Http\Request;
use App\Userlevel;
use Illuminate\Support\Facades\Gate;

class TenantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('admin-only', Auth::user()->userlevel->userLevel))
        {
            $userA=Tenant::all();
            $i=1;
            return view('tenant.list', compact('userA', 'i'));
        }
        $houses=HouseUnit::where('owner_id', '=', Auth::user()->id)->pluck('id')->toArray();
        $userA=Tenant::wherein('house_id', $houses)->get();
        // dd($userA);
        $i=1;
        return view('tenant.list', compact('userA', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::allows('admin-only', Auth::user()->userlevel->userLevel)){
            $houses=HouseUnit::all();
            return view('tenant.create', compact('houses'));
        }
        $houses=HouseUnit::where('owner_id', '=', Auth::User()->id)->get();
        return view('tenant.create', compact('houses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $this->separate($request);
        
        if(Gate::allows('admin-only', Auth::user()->userlevel->userLevel)||$this->checkExist($request)||$id[1]==Auth::User()->id)
        {
            $tenant= new Tenant();
            $tenant->name=$request->get('name');
            $tenant->passport=$request->get('passport');
            $tenant->citizenship=$request->get('citizen');
            $tenant->phone=$request->get('phone');
            $tenant->duration=$request->get('duration');

            if($request->get('status')==1)
            {
                $house=HouseUnit::find($request->get('houseid'));
                $house->status=1;
                $house->save();
                $tenant->houseunits()->associate($house);
                $tenant->save();
                return redirect('/tenant')->with('success', 'Tenant Registered');
            }

            $house=HouseUnit::find($id[0]);
            $house->status=1;
            $house->save();
            $tenant->houseunits()->associate($house);
            $tenant->save();

            return redirect('/tenant')->with('success', 'Tenant Registered');


        }else
        {
            return redirect('/house')->with('danger', 'Please Contact Admin first');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tenant  $tenant
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tenant=Tenant::with('houseunits.user')->where('house_id', '=', $id)->first();
        
        // dd($tenant);
        return view('tenant.show', compact('tenant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tenant  $tenant
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $tenant=Tenant::with('houseunits')->where('id', '=', $id)->first();
        $houses=HouseUnit::where('owner_id', '=', Auth::User()->id)->get();
        
        // dd($tenant);
        return view('tenant.edit', compact('tenant', 'houses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tenant  $tenant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tenant=Tenant::find($id);

        // $request->validate([
        //     'name' => 'required | max:1',
        //     'email' => 'required | email',
        //     'department_id' => 'required'
        // ]);

        // $request->validate();

        $id=$this->separate($request);

        $tenant->passport=$request->get('passport');
        $tenant->name=$request->get('name');
        $tenant->phone=$request->get('phone');
        $tenant->duration=$request->get('duration');
        $tenant->citizenship=$request->get('citizen');
        $house=HouseUnit::find($id[0]);
        $tenant->houseunits()->associate($house);
        $tenant->save();



        

        return redirect('/tenant')->with('success', 'Tenant has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tenant  $tenant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tenant=Tenant::with('houseunits')->find($id);
        $tenant->houseunits['status']=0;
        $tenant->houseunits->save();

        $tenant->delete();
        return redirect('/tenant')->with('success', 'Tenant has been removed');
    }
    public function checkExist(Request $request)
    {
        if($request->exists('owner')){
            return true;
        }
        return false;
    }
    public function separate(Request $request)
    {
        $id=explode('|', $request->get('status'));
        return $id;
    }
}
