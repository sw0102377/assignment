<?php

namespace App\Http\Controllers;

use Auth;
use App\userDetails;
use App\HouseUnit;
use Illuminate\Http\Request;
use App\Userlevel;
use App\User;
use App\Tenant;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\TenantController;

class HouseUnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user = Auth::user();

        //Details Check
        if(!userDetails::where('user_id', '=', $user->id)->count() > 0){
            return view('details.createNew');
        }
        
        //Gate Check
        $userlevel=Auth::user()->userlevel->userLevel;
        
        if(Gate::allows('admin-only', $userlevel)){
            $houses=HouseUnit::with('user')->get();
            $i=1;
            return view('house.list', compact('houses', 'i'));
        }

        $houses=HouseUnit::with('user')->where('owner_id', '=', $user->id)->get();
        $i=1;
        return view('house.list', compact('houses', 'i'));
        


        // $houses=HouseUnit::with('user')->get();
        // $i=1;
        // return view('house.list', compact('houses', 'i'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $owners=User::all();
        return view('house.create', compact('owners'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        if(Gate::allows('admin-only', Auth::user()->userlevel->userLevel) || $request->get('owner')==Auth::User()->id ){

        // $request->validate([
        //     'phone' => 'required|unique:user_Details',
        //     'citizenship' => 'required|min:1|max:100',
        // ]);
            


            $house= new HouseUnit();
            $house->housenumber=$request->get('housenumber');
            $house->level=$request->get('houselevel');
            $house->block=$request->get('houseblock');
            $house->status=$request->get('status');

            $owner=User::find($request->get('owner'));
            $house->user()->associate($owner);

            $house->save();
            $request->merge(['houseid' => $house->id]);
            if($request->get('status')==1){
                app(\App\Http\Controllers\TenantController::class)->store($request);
            }

            return redirect('/house')->with('success', 'Your House has been listed');
        }else{
            return redirect('/house')->with('danger', 'Please Contact Admin first');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HouseUnit  $houseUnit
     * @return \Illuminate\Http\Response
     */
    public function show(HouseUnit $houseUnit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HouseUnit  $houseUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(HouseUnit $houseUnit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HouseUnit  $houseUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HouseUnit $houseUnit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HouseUnit  $houseUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $house=HouseUnit::find($id);
        $tenant=Tenant::where('house_id', '=', $house->id)->get();
        foreach ($tenant as $t) {
            $t->delete();
        }
        $house->delete();
        return redirect('/house')->with('success', 'Your House has been removed');
    }


}
