<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenant extends Model
{
    use SoftDeletes;
    
    public function houseunits() {
        return $this->belongsTo('App\HouseUnit', 'house_id');
    }
}
