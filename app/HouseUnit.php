<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HouseUnit extends Model
{
    use SoftDeletes;

    public function tenant(){
        return $this->hasOne('App\HouseUnit');
    }


    public function user(){
        return $this->belongsTo('App\User', 'owner_id');
    }








}
